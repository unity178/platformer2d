using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rigibody2D;
    private CapsuleCollider2D playerCollider;
    private SkeletonAnimation playerSkeletonAnimation;

    [SerializeField] private Transform startPoint;
    [SerializeField] private Indicator indicator;
    [SerializeField] private float _speed;
    [SerializeField] private LayerMask _ground;
    [SerializeField] private float _slopeLimit;
    [SerializeField] private float _slopeCheckDistance;

    //Movement
    private float movementInput;
    private bool facingRight;
    private Vector3 directionRight = new Vector3(1f, 1f, 1f);
    private Vector3 directionLeft = new Vector3(-1f, 1f, 1f);
    

    private bool sliding;
    
    private void Awake()
    {
        rigibody2D = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<CapsuleCollider2D>();
        playerSkeletonAnimation = GetComponent<SkeletonAnimation>();
        facingRight = true;
        sliding = false;
    }

    void Update()
    {
        SetSliding();
        if (sliding)
        {
            playerSkeletonAnimation.AnimationName = "idle";
        }
        else
        {
            Move();
        }
    }

    private void FixedUpdate()
    {
        
        if (sliding)
        {
            movementInput = 0f;
        }
    }

    public void SetMovementInput(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>().x;
    }

    private void Move() 
    {   

        Vector3 currentPosition = transform.position;
        currentPosition.x += movementInput * _speed * Time.deltaTime;
        rigibody2D.MovePosition(currentPosition);
        if (Mathf.Abs(movementInput) > 0.5f)
        {
            playerSkeletonAnimation.AnimationName = "run";
            FlipX();
        }
        else
        {
            playerSkeletonAnimation.AnimationName = "idle";
        }    
    }

    private void FlipX() 
    {
        if (facingRight && movementInput < 0) 
        {
            transform.localScale = directionLeft;
            facingRight = false;
        }else if(!facingRight && movementInput > 0) 
        {
            transform.localScale = directionRight;
            facingRight = true;
        }
    }

    private void SetSliding()
    {
        Vector2 position2D = new Vector2(transform.position.x, transform.position.y) + (Vector2.up * 3);

        Vector2 bottomLeftPoint = position2D;
        bottomLeftPoint.x -= (playerCollider.size.x / 2);

        Vector2 bottomRightPoint = position2D;
        bottomRightPoint.x += (playerCollider.size.x / 2);
        
        RaycastHit2D hitOne = Physics2D.Raycast(bottomLeftPoint, Vector2.down, _slopeCheckDistance, _ground);
        RaycastHit2D hitTwo = Physics2D.Raycast(bottomRightPoint, Vector2.down, _slopeCheckDistance, _ground);


        if (hitOne && hitTwo)
        {
            float angle1 = Vector2.Angle(hitOne.normal, Vector2.up);
            float angle2 = Vector2.Angle(hitTwo.normal, Vector2.up);

            sliding = (angle1 > _slopeLimit && angle2 > _slopeLimit);
            if(indicator != null)
                indicator.SetColor(sliding ? Color.red : Color.green);
            

            Debug.DrawRay(hitOne.point, hitOne.normal, Color.red, 6f);
            Debug.DrawRay(hitTwo.point, hitTwo.normal, Color.red, 6f);


        }
    }

    public void ReturnToStart()
    {
        transform.position = startPoint.position;
    }
}
