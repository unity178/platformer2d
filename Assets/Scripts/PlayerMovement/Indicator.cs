using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicator : MonoBehaviour
{
    private Material material;
    private MeshRenderer meshRenderer;
    public void SetColor(Color color) 
    {
        material.color = color; 
    }
    void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        material = meshRenderer.material;
    }

}
